#[macro_use]
extern crate clap;
extern crate hound;
extern crate unicode_segmentation;
#[macro_use]
extern crate lazy_static;

mod phonemes;
mod acoustics;

use std::collections::LinkedList;

#[derive(Debug)]
pub struct Node(String, NodeType);

#[derive(Debug, PartialEq, Eq)]
pub enum NodeType {
    Phoneme,
    Intonation,
    Modifier,
    Pause,
    Unknown
}

fn main() {
    let matches = clap_app!(eya4 =>
        (version: "0.2")
        (author: "msifeed")
        (about: "Компиляционный синтезатор речи")
        (@arg INPUT: +required "Sets the input text file")
        (@arg OUTPUT: "Sets the output .wav file")
    ).get_matches();

    use std::io::prelude::*;
    use std::fs::File;
    let mut input_file = File::open(matches.value_of("INPUT").unwrap()).expect("Cannot open file");
    let mut input = String::new();
    input_file.read_to_string(&mut input).expect("File is not UTF-8");

    let output_file = matches.value_of("OUTPUT").unwrap_or("compilation.wav");

    let supported_chars = " ,:+=^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    let filtered = input.chars().filter(|&c| supported_chars.contains(c)).collect::<String>();
    let phonemes = phonemes::into_phonemes(&filtered);
    let samples = acoustics::into_samples(&phonemes);

    println!("input: {}", input);
    println!("filtered: {}", filtered);
    println!("phonemes: {:?}", phonemes);
    println!("samples len: {}", samples.len());

    compile(&samples, output_file);
}

fn compile(samples: &LinkedList<i16>, filename: &str) {
    let spec = hound::WavSpec {
        channels: 2,
        sample_rate: 44100,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    };

    let mut writer = hound::WavWriter::create(filename, spec).unwrap();
    for s in samples {
        writer.write_sample(*s).unwrap();
    }
    writer.finalize().unwrap();
}