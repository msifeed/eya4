use std::collections::LinkedList;

use super::{Node, NodeType};
use super::acoustics;

#[inline]
fn phoneme_rules(inp: &str, pos: usize, word: &str) -> Vec<Node> {
    let pause = match inp {
        "," | ":" => "pause-05",
        _ => ""
    };

    if pause != "" {
        return vec![Node(pause.to_string(), NodeType::Pause)];
    }

    let phoneme = match inp {
        "а" => "a",
        "б" => "b",
        "в" => "v",
        "г" => "g",
        "д" => "d",
        "е" => "j e",
        "ё" => "j o",
        "ж" => "z-",
        "з" => "z",
        "и" => "i",
        "й" => "j",
        "к" => "k",
        "л" => "l",
        "м" => "m",
        "н" => "n",
        "о" => "o",
        "п" => "p",
        "р" => "r",
        "с" => "s",
        "т" => "t",
        "у" => "u",
        "ф" => "f",
        "х" => "h",
        "ц" => "ts",
        "ч" => "tc",
        "ш" => "c;",
        "щ" => "z;",
        "ы" => "i-",
        "э" => "e",
        "ю" => "j u",
        "я" => "j a",
        _ => ""
    };

    if phoneme != "" {
        return phoneme.split(" ").map(|ph| Node(ph.to_string(), NodeType::Phoneme)).collect();
    }

    let modifier = match inp {
        "ь" => "'",
        "ъ" => "ъ",
        _ => ""
    };

    if modifier != "" {
        return vec![Node(modifier.to_string(), NodeType::Modifier)];
    }

    let intonation = match inp {
        "+" | "=" | "^" => inp,
        _ => ""
    };

    if intonation != "" {
        return vec![Node(intonation.to_string(), NodeType::Intonation)];
    }

    return vec![Node(inp.to_string(), NodeType::Unknown)];
}

pub fn into_phonemes(input: &String) -> LinkedList<Node> {
    use unicode_segmentation::UnicodeSegmentation;

    input.to_lowercase().split_whitespace()
        .map(|word| {
            let mut nodes = word.graphemes(true).enumerate()
                .map(|(i, gr)| phoneme_rules(gr, i, word))
                .flat_map(|ph| ph)
                .collect::<LinkedList<Node>>();
            nodes.push_back(Node("pause-2".to_string(), NodeType::Pause));
            nodes
        })
        .flat_map(|word_ph| word_ph)
        .fold(LinkedList::new(), |mut acc, mut node| {
            if let Some(mut prev) = acc.back_mut() {
                match node.1 {
                    NodeType::Modifier => {
                        // Объединение модификатора c предыдыдущей фонемой
                        let new_phoneme = prev.0.clone() + node.0.as_str();
                        if acoustics::DICT.contains_key(&new_phoneme) {
                            prev.0 = new_phoneme;
                        }
                    },
                    NodeType::Phoneme => {
                        if node.0 == "j" {
                            // Смягчение предыдущей фонемы
                            let new_phoneme = prev.0.clone() + "'";
                            if acoustics::DICT.contains_key(&new_phoneme) {
                                prev.0 = new_phoneme;
                                node.1 = NodeType::Modifier;
                            }
                        }
                    }
                    _ => ()
                }
            }

            match node.1 {
                NodeType::Phoneme | NodeType::Pause => acc.push_back(node),
                _ => ()
            }

            acc
        })
}