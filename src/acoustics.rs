use std::collections::BTreeMap;
use std::collections::LinkedList;
use hound;

use super::Node;

macro_rules! setup_samples_dict {
    ($($a:expr),*) => {
        lazy_static! {
            pub static ref DICT: BTreeMap<String, LinkedList<i16>> = {
                let mut dict = BTreeMap::<String, LinkedList<i16>>::new();
                $(
                    let bytes = include_bytes!(concat!("samples/", $a, ".wav"));
                    let mut reader = hound::WavReader::new(bytes as &[u8]).unwrap();
                    let sample = reader.samples::<i16>().map(|s| s.unwrap()).collect();
                    dict.insert($a.to_string(), sample);
                )*
                dict
            };
        }
    };
}

setup_samples_dict!["pause-05", "pause-2", "a", "b", "b'", "s", "s'", "c;", "d", "d'", "e",
    "f", "f'", "g", "g'", "h", "h'", "i", "i-", "j", "k", "k'", "l", "l'", "m", "m'", "n", "n'",
    "o", "p", "p'", "r", "r'", "t", "t'", "tc", "ts", "u", "v", "v'", "z", "z'", "z;", "zz"];

pub fn into_samples(phonemes: &LinkedList<Node>) -> LinkedList<i16> {
    phonemes.iter()
        .filter(|&ph| DICT.contains_key(&ph.0))
        .map(|ph| DICT.get(&ph.0).unwrap())
        .fold(LinkedList::new(), |mut acc, sp| {
            acc.append(&mut sp.clone());
            acc
        })
}